package com.jetbrains.handson.routes

import Customer
import customerStorage
import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.response.respondText
import io.ktor.routing.Route
import io.ktor.routing.delete
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.put
import io.ktor.routing.route
import io.ktor.routing.routing

fun Application.registerCustomerRoutes() {
    routing {
        customerRouting()
    }
}

fun Route.customerRouting() {
    route("/customer") {
        get {
            if (customerStorage.isNotEmpty()) {
                call.respond(customerStorage)
            } else {
                call.respondText("No customers found", status = HttpStatusCode.NotFound)
            }
        }
        get("{id}") {
            val id = call.parameters["id"] ?: return@get call.respondText(
                "Missing or malformed id",
                status = HttpStatusCode.BadRequest
            )
            val customer =
                customerStorage.find { it.id == id } ?: return@get call.respondText(
                    "No customer with id $id",
                    status = HttpStatusCode.NotFound
                )
            call.respond(customer)
        }
        post {
            val customer = call.receive<Customer>()
            if (customerStorage.find { it.id == customer.id } != null) {
                return@post call.respondText(
                    "Customer with id ${customer.id} already exists",
                    status = HttpStatusCode.BadRequest
                )
            } else {
                customerStorage.add(customer)
                call.respondText("Customer stored correctly", status = HttpStatusCode.Created)
            }
        }
        put("{id}") {
            val updatedCustomer = call.receive<Customer>()
            if (customerStorage.find { it.id == updatedCustomer.id } == null) {
                return@put call.respondText(
                    "Customer with id ${updatedCustomer.id} does not exists",
                    status = HttpStatusCode.BadRequest
                )
            } else {
                customerStorage.find { it.id == updatedCustomer.id }?.let {
                    it.email = updatedCustomer.email
                    it.firstName = updatedCustomer.firstName
                    it.lastName = updatedCustomer.lastName
                }
                call.respondText("Customer updated correctly", status = HttpStatusCode.Created)
            }
        }
        delete("{id}") {
            val id = call.parameters["id"] ?: return@delete call.respond(HttpStatusCode.BadRequest)
            if (customerStorage.removeIf { it.id == id }) {
                call.respondText("Customer removed correctly", status = HttpStatusCode.Accepted)
            } else {
                call.respondText("Not Found", status = HttpStatusCode.NotFound)
            }
        }
    }
}
