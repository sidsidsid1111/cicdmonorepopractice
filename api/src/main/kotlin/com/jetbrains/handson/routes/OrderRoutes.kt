package com.jetbrains.handson.routes

import com.jetbrains.handson.models.Order
import com.jetbrains.handson.models.orderStorage
import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.response.respondText
import io.ktor.routing.Route
import io.ktor.routing.delete
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.put
import io.ktor.routing.route
import io.ktor.routing.routing

fun Application.registerOrderRoutes() {
    routing {
        orderRoute()
        totalizeOrderRoute()
    }
}

fun Route.orderRoute() {
    route("/order") {
        get {
            if (orderStorage.isNotEmpty()) {
                call.respond(orderStorage)
            }
        }
        get("{id}") {
            val id = call.parameters["id"]
                ?: return@get call.respondText("Bad Request", status = HttpStatusCode.BadRequest)
            val order = orderStorage.find { it.number == id } ?: return@get call.respondText(
                "Not Found",
                status = HttpStatusCode.NotFound
            )
            call.respond(order)
        }
        post {
            val order = call.receive<Order>()
            if (orderStorage.find { it.number == order.number } != null) {
                return@post call.respondText(
                    "Order with number ${order.number} already exists",
                    status = HttpStatusCode.BadRequest
                )
            } else {
                orderStorage.add(order)
                call.respondText("Order stored correctly", status = HttpStatusCode.Created)
            }
        }
        put("{id}") {
            val updatedOrder = call.receive<Order>()
            if (orderStorage.find { it.number == updatedOrder.number } == null) {
                return@put call.respondText(
                    "Order with number ${updatedOrder.number} does not exists",
                    status = HttpStatusCode.BadRequest
                )
            } else {
                orderStorage.find { it.number == updatedOrder.number }?.let { it.contents = updatedOrder.contents }
                call.respondText("Order updated correctly", status = HttpStatusCode.Created)
            }
        }
        delete("{id}") {
            val id = call.parameters["id"] ?: return@delete call.respond(HttpStatusCode.BadRequest)
            if (orderStorage.removeIf { it.number == id }) {
                call.respondText("Order removed correctly", status = HttpStatusCode.Accepted)
            } else {
                call.respondText("Not Found", status = HttpStatusCode.NotFound)
            }
        }
    }
}

fun Route.totalizeOrderRoute() {
    get("/order/{id}/total") {
        val id = call.parameters["id"] ?: return@get call.respondText("Bad Request", status = HttpStatusCode.BadRequest)
        val order = orderStorage.find { it.number == id } ?: return@get call.respondText(
            "Not Found",
            status = HttpStatusCode.NotFound
        )
        val total = order.contents.map { it.price * it.amount }.sum()
        call.respond(total)
    }
}
