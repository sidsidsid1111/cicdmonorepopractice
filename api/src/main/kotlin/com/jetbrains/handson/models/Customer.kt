import kotlinx.serialization.Serializable

val customerStorage = mutableListOf<Customer>()

@Serializable
data class Customer(val id: String, var firstName: String, var lastName: String, var email: String)
