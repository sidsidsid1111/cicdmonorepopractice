#!/bin/bash

apk add wget && apk add unzip
wget https://github.com/viclovsky/swagger-coverage/releases/download/1.4.2/swagger-coverage-1.4.2.zip
unzip swagger-coverage-1.4.2.zip

openApi="$(pwd)/api/openapi.json"
outputFolder="$(pwd)/tests/swagger-coverage-output"

cd swagger-coverage-commandline-1.4.2/bin
./swagger-coverage-commandline -s $openApi -i $outputFolder

cd ../../

mkdir public && mkdir public/swagger-api-coverage
cp -R swagger-coverage-commandline-1.4.2/* public/swagger-api-coverage
