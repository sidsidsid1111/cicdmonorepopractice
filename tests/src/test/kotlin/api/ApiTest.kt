package api

import Customer
import com.github.viclovsky.swagger.coverage.SwaggerCoverageRestAssured
import com.jetbrains.api.specifications.Specifications
import io.restassured.RestAssured
import io.restassured.filter.log.RequestLoggingFilter
import io.restassured.filter.log.ResponseLoggingFilter
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.apache.http.HttpStatus
import org.junit.Before
import org.junit.Test

class ApiTest {
    @Before
        fun setup() {
            RestAssured.requestSpecification = Specifications.baseReq()
            RestAssured.responseSpecification = Specifications.baseResp()
            RestAssured.filters(RequestLoggingFilter(), ResponseLoggingFilter(), SwaggerCoverageRestAssured())
        }

    @Test
    fun customerCreationTest() {
        val customer = Customer("101", "Fred", "Hellen", "fred.hellen@gmail.com")

        RestAssured
            .given()
            .body(Json.encodeToString(customer))
            .post("/customer")
            .then()
            .assertThat()
            .statusCode(HttpStatus.SC_CREATED)
    }
}
