package ui

import com.codeborne.selenide.Configuration
import com.jetbrains.Config
import com.jetbrains.ui.pages.MainPage
import org.junit.Before
import org.junit.Test

class UiTest {
    @Before
    fun setup() {
        if (Config.remoteUrl.isNotEmpty()) Configuration.remote = Config.remoteUrl
        Configuration.browser = Config.browser
        Configuration.baseUrl = Config.uiUrl
    }


    @Test
    fun entryTest() {
        MainPage()
            .setJournalEntry("My headline", "My text")
            .clickSubmit()
            .checkHeader("Thanks for submitting your entry!")
    }
}
