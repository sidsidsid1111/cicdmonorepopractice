package com.jetbrains.api.specifications

import com.jetbrains.Config
import io.restassured.builder.RequestSpecBuilder
import io.restassured.builder.ResponseSpecBuilder
import io.restassured.filter.log.LogDetail
import io.restassured.http.ContentType
import io.restassured.specification.RequestSpecification
import io.restassured.specification.ResponseSpecification

object Specifications {
    fun baseReq(): RequestSpecification {
        val builder = RequestSpecBuilder()
        builder.setBaseUri(Config.apiUrl)
        builder.setAccept(ContentType.JSON)
        builder.setContentType(ContentType.JSON)
        builder.log(LogDetail.ALL)
        return builder.build()
    }

    fun baseResp(): ResponseSpecification {
        val builder = ResponseSpecBuilder()
        builder.log(LogDetail.ALL)
        return builder.build()
    }
}
