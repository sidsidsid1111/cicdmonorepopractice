package com.jetbrains

object Config {
    val apiUrl =  System.getProperty("apiUrl") ?: "http://0.0.0.0:8081"
    val uiUrl =  System.getProperty("uiUrl") ?: "http://0.0.0.0:8082"
    val remoteUrl = System.getProperty("remoteUrl") ?: ""
    val browser = System.getProperty("browser") ?: "chrome"
}
