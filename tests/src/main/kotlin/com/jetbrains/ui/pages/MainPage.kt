package com.jetbrains.ui.pages

import com.codeborne.selenide.Selenide
import com.codeborne.selenide.Selenide.`$`
import com.codeborne.selenide.SelenideElement

class MainPage {
    init {
        Selenide.open("/")
    }

    private val inputHeadline: SelenideElement by lazy { `$`("input[type='text']") }
    private val inputText: SelenideElement by lazy { `$`("textarea[name='body']") }
    private val submitButton: SelenideElement by lazy { `$`("input[type='submit']") }
    private val header: SelenideElement by lazy { `$`("h1") }

    fun setJournalEntry(headline: String, text: String) = apply {
        inputHeadline.setValue(headline)
        inputText.setValue(text)
    }

    fun clickSubmit() = apply {
        submitButton.click()
    }

    fun checkHeader(message: String) = apply {
        assert(message == header.text)
    }
}
